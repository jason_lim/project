import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnitTest {
	@Test
	public void testForAdding2Numbers() {
		Prog1 tester = new Prog1(); // Prog1 is tested

		// assert statements
		assertEquals(5, tester.add2Numbers(1, 4), "1 + 4 must be 5");
		assertEquals(1, tester.add2Numbers(4, 2), "4 + 2 must be 6");
	}
}
